package dailyPlanner;

import java.util.Locale;
import java.util.Scanner;

public class DaiLyPlanner {
    public static void main(String[] args) {
        String[][] scedule = new String[7][2];
        fillScedule(scedule);

        int day = -1;
        while (day != 7) {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Please, input the day of the week:");
            String str = scanner.nextLine().trim().toLowerCase();
            int indexCharge = str.indexOf("change");
            int indexReschedule = str.indexOf("reschedule");
            if (indexCharge != -1){
                day = findDay(str.substring(6).trim());
                if (day > -1 && day < 7){
                    System.out.printf("Please, input new tasks for %s", scedule[day][0]);
                    scedule[day][1] = scanner.nextLine();
                }
            } else if (indexReschedule != -1){
                day = findDay(str.substring(10).trim());
                if (day > -1 && day < 7){
                    System.out.printf("Please, input new tasks for %s", scedule[day][0]);
                    scedule[day][1] = scanner.nextLine();
                }
            } else {
                day = findDay(str); // return the number of day, if "exit"  return "7", else "-1"
                if (day > -1 && day < 7){
                    System.out.printf("Your tasks for %s: %s", scedule[day][0], scedule[day][1]);
                    System.out.println();
                } else {
                    System.out.println("Sorry, I don't understand you, please try again.");
                }
            }
        }
    }
    public static int findDay (String str){
        switch (str){
            case "sunday": {
                return 0;
            }
            case "monday": {
                return 1;
            }
            case "tuesday": {
                return 2;
            }
            case "wednesday": {
                return 3;
            }
            case "thursday": {
                return 4;
            }
            case "friday": {
                return 5;
            }
            case "saturday": {
                return 6;
            }
            case "exit": {
                return 7;
            }
            default:
                return -1;
        }
    }


    public static void fillScedule(String[][] scedule){
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "do something";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "";
        scedule[5][0] = "Friday";
        scedule[5][1] = "";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "";
    }
}
