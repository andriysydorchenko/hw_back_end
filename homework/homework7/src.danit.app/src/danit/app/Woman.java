package src.danit.app;

public final class Woman extends Human{
    @Override
    public void greetPet() {
        System.out.printf("Hello, %s" , this.getPet().getNickname());
        System.out.println();
    }
    public void makeup(){
        System.out.println("I creating makeup");
    }
}
