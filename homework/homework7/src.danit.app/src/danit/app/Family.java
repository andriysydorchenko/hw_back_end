package src.danit.app;


import java.util.*;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children = new ArrayList<>();
    private Set<Pet> pets = new HashSet();
    private int point = 0;
    public Family(Human mother, Human father){
        this.mother = mother;
        this.father = father;
    }

    //    getters
    public Human getMother() {
        return mother;
    }
    public Human getFather() {
        return father;
    }
    public List<Human>  getChildren() {
        return children;
    }
    public Set<Pet> getPet() {
        return pets;
    }

    //    setters
    public void setChildren(List<Human>  children) {
        this.children = children;
    }
    public void setFather(Human father) {
        this.father = father;
    }
    public void setMother(Human mother) {
        this.mother = mother;
    }
    public void setPet(Pet pet) {
        this.pets.add(pet);
        mother.setPet(pet);
        father.setPet(pet);
        for (Human child: children) {
            child.setPet(pet);
        }
    }

    public void addChild(Human child) {
        children.add(child);
        child.setFamily(this);
    }
    public boolean deleteChild(int indexChild) {
        try{
            children.remove(indexChild);
            return true;
        }catch (IndexOutOfBoundsException e){
            System.out.println(e.getMessage());
            return false;
        }
    }
    public boolean deleteChild(Human child) {
        return children.remove(child);
    }
    public int getCountFamily(){
        return 2+children.size();
    }
    @Override
    public String toString(){
        String str = """
                        father: %s
                        mother: %s
                        children: %s
                     """.formatted(father.toString(), mother.toString(), children.toString());
        return str;
    }
    @Override
    protected void finalize(){
        System.out.println("deleted exemplar of class Family");
    }

}
