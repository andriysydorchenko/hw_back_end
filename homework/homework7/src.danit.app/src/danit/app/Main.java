package src.danit.app;
import java.util.Arrays;


public class Main{
    public static void main(String[] args) {
        Human father1 = new Human();
        System.out.println(father1);

        for (int i = 0; i < 100_000; i++) {
            Human father = new Human();
        }
    }

    public String[][] createSchedule () {
        String[][] schedule = new String[7][7];
        DayOfWeek[] days = DayOfWeek.values();
        int i = 0;
        for (DayOfWeek day: days) {
            schedule[i][0] = day.name();
            schedule[i][1] = "do" + 1;
        }
        return schedule;
    }
}