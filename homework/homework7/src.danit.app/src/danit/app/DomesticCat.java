package src.danit.app;

public class DomesticCat extends Pet implements PetFoul{
    private Species species = Species.DOMESTIC_CAT;
    @Override
    public void respond() {

    }
    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
}
