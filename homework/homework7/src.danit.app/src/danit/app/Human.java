package src.danit.app;

import java.util.HashMap;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Family family;
    private Human mother;
    private Human father;
    private HashMap<String, String> schedule = new HashMap<String, String>();

    //constructors
    public Human(){
    }
    public Human(String name, String surname, int year){
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name, String surname, int year,  Human mother, Human father){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }
    public Human(String name, String surname, int year, int iq, Human mother, Human father, Pet pet, HashMap<String, String> schedule, Family family){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
        this.iq = iq;
        this.pet = pet;
        this.schedule  = schedule;
        this.family = family;
    }

    public void greetPet() {
        System.out.printf("Привет, %s" , pet.getNickname());
        System.out.println();
    }
    public  void describePet() {
        String s = String.format("У меня есть %s, ему %d лет, он %s", pet.getSpecies(), pet.getAge(), pet.getTrickLevel());
        System.out.println(s);
    }
    @Override
    public String toString(){
        String nameStr  = name == null ? "no name" : name;
        String surnameStr  = surname == null ? "no surname" : surname;
        String motherStr  = mother == null ? "no mother" : mother.getFullName();
        String fatherStr  = father == null ? "no father" : father.getFullName();
        String petString = pet == null ? " no pets" : pet.toString();

        String s = String.format("Human{name='%s', surname='%s', year=%d, iq=%d, mother=%s, father=%s, pet=%s}", nameStr, surnameStr, year, iq, motherStr, fatherStr, petString);
        return s;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human that = (Human) o;

        if (!name.equals(that.name)) return false;
        if (!surname.equals(that.surname)) return false;
        if (year != that.year) return false;
        if (iq != that.iq) return false;
        if (mother != that.mother) return false;
        return father == that.father;
    }
    @Override
    public int hashCode() {
        int result = name == null ? 0 : name.hashCode();
        result = 31 * result + (surname == null ? 0 : surname.hashCode());
        result = 31 * result + year;
        result = 31 * result + iq;
        result = 31 * result + (mother == null ? 0 : mother.hashCode());
        result = 31 * result + (father == null ? 0 : father.hashCode());
        return result;
    }
    @Override
    protected void finalize(){
        System.out.println("deleted exemplar of class Human");
    }
    //    getters
    public String getFullName() {
        return name + " " + surname;
    }
    public Human getFather() {
        return father;
    }
    public Human getMother() {
        return mother;
    }
    public Family getFamily() {
        return family;
    }
    public int getIq() {
        return iq;
    }
    public int getYear() {
        return year;
    }
    public Pet getPet() {
        return pet;
    }
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    //    setter
    public void setName(String name){
        this.name = name;
    }
    public void setSurname(String surname){this.surname = surname;}
    public void setYear(int year){ this.year = year;}
    public void setIq(int iq){this.iq = iq;}
    public void setPet(Pet pet){this.pet = pet;}
    public void setFamily(Family family){
        this.family = family;
    }
    public void setMother(Human mother){this.mother = mother;}
    public void setFather(Human father){this.father = father;}
    public void setSchedule(HashMap<String, String> schedule) {
        this.schedule = schedule;
    }
}

