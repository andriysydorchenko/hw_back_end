package com.danit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class FamilyController  {
    FamilyService familyService = new FamilyService();
    public List<Family> getAllFamilies(){
        return familyService.getAllFamilies();
    }
    public List<Family> getFamiliesBiggerThan(int countMembers) {
        return familyService.getFamiliesBiggerThan(countMembers);
    }
    public List<Family> getFamiliesLessThan(int countMembers) {
        return familyService.getFamiliesLessThan(countMembers);
    }
    public int countFamiliesWithMemberNumber(int countMembers) {
        return familyService.countFamiliesWithMemberNumber(countMembers);
    }
    public void createNewFamily(Human mother, Human father){
        familyService.createNewFamily(mother, father);
    }
    public boolean deleteFamilyByIndex(int index){
        return familyService.deleteFamilyByIndex(index);
    }
    public Family bornChild(Family family, String manName, String womanName) throws CloneNotSupportedException {
        Family newFamily = null;
        try{
            newFamily = familyService.bornChild(family, manName, womanName);
        } catch (FamilyOverflowException e){
            System.out.println(e.getMessage());
        }
        return newFamily;
    }
    public Family adoptChild(Family family, Human child) throws CloneNotSupportedException {
        Family newFamily = null;
        try{
            newFamily = familyService.adoptChild(family,child);
        } catch (FamilyOverflowException e){
            System.out.println(e.getMessage());
        }
        return newFamily;
    }
    public void deleteAllChildrenOlderThen(int age){
        familyService.deleteAllChildrenOlderThen(age);
    }
    public int count(){
        return familyService.count();
    }
    public Family getFamilyByIndex(int index) {
        return familyService.getFamilyByIndex(index);
    }
    public Set<Pet> getPets(int indexFamily) {
        return familyService.getPets(indexFamily);
    }
    public void addPet(int indexFamily, Pet pet){
        familyService.addPet(indexFamily, pet);
    }
    public void loadFamilies() {
        familyService.loadFamilies();
    }
    public void getFamiliesFromFile(){
        familyService.getFamiliesFromFile();
    }
}
