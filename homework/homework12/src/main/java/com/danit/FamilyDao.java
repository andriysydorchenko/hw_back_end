package com.danit;

import java.util.List;
import java.util.function.Predicate;

public interface FamilyDao {
    List<Family> getAllFamilies();
    Family getFamilyByIndex(int index);
    boolean deleteFamily(int index);
    boolean deleteFamily(Family family);
    void saveFamily(Family family);
    public List<Family> getFilteredFamilies(Predicate<Family> predicate);
    List<Family> getData();
    void loadData(List<Family> families);
}
