package com.danit;

import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Set;

public class UtilsCreateTestFamily {


    static void createTestFamily(FamilyController familyController) throws CloneNotSupportedException {
        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");
        LocalDate now = LocalDate.now();
        long l = now.atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
        Pet roboCat1 = new RoboCat( "RoboCat",2, 80,  habits);
        Pet dog1 = new Dog("dog1", 5, 15, habits);
        Pet dog2 = new Dog("dog2", 2, 90, habits);


        Human mother1 = new Human("mother1", "surname", l, null, null);
        Human father1 = new Human("father1", "surname", l, null, null);

        Human child1 = new Woman("nameChild1", "surnameChild1", l, 180, mother1, father1, null, null, null);
        Human child2 = new Man("nameChild2", "surnameChild2", l, 120, mother1, father1, null, null, null);

        Woman woman1 = new Woman("nameMother1", "surnameMother1", l, 160, mother1, father1, roboCat1, null, null);
        Human man1 = new Man("nameFather1", "surnameFather1", l, 160, mother1, father1, dog1, null, null);

        Human woman2 = new Woman("nameMother2", "surnameMother2", l, 160, mother1, father1, null, null, null);
        Human man2 = new Man("nameFather2", "surnameFather2", l, 160, mother1, father1, dog1, null, null);

        Human woman3 = new Woman("nameMother2", "surnameMother2", l, 160, mother1, father1, null, null, null);
        Human man3 = new Man("name", "surname", l, 200);


        familyController.createNewFamily(woman1,man1);
        Family family1 = new Family(woman1, man1);
        familyController.bornChild(family1,"ChildMan1", "ChildWoman1");
        familyController.adoptChild(family1, child1);
        familyController.adoptChild(family1, child2);
        familyController.createNewFamily(woman2, man2);
        Family family2 = new Family(woman2, man2);
        familyController.bornChild(family2,"ChildMan2", "ChildWoman2");
    }
}
