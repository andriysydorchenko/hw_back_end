package com.danit;

import java.util.HashMap;

public final class Woman extends Human {
    public Woman(String name, String surname, long birthDate, int iq, Human mother, Human father, Pet pet, HashMap<String, String> schedule, Family family) {
        super(name, surname, birthDate, iq, mother, father, pet, schedule, family);
    }

    public Woman(String name, String surname, long birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    @Override
    public void greetPet() {
        System.out.printf("Hello, %s" , super.getPet().getNickname());
        System.out.println();
    }
    public void makeup(){
        System.out.println("I creating makeup");
    }
}
