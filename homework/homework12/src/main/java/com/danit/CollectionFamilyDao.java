package com.danit;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class CollectionFamilyDao implements FamilyDao{
    private List <Family> familyList = new ArrayList<>();
    File file = new File("homework/homework12/src/main/resources/familyDb.txt");

    @Override
    public List<Family> getAllFamilies() {
        return familyList;
    }
    @Override
    public Family getFamilyByIndex(int index) {
        return familyList.get(index);
    }
    @Override
    public boolean deleteFamily(int index) {
        return familyList.remove(index) != null;
    }
    @Override
    public boolean deleteFamily(Family family) {
        int index = familyList.indexOf(family);
        if(index > -1) {
            familyList.remove(index);
            return true;
        }
        return false;
    }
    @Override
    public void saveFamily(Family family) {
        int index = familyList.indexOf(family);
        if(index > -1) {
            familyList.set(index, family);
        }else {
            familyList.add(family);
        }
    }
    @Override
    public List<Family> getFilteredFamilies(Predicate<Family> predicate) {
        return familyList.stream().filter(predicate).collect(Collectors.toList());
    }
    public List<Family> getData(){
        FileInputStream fileInputStream = null;
        List<Family> families = null;
        try {
            fileInputStream = new FileInputStream(file);
            ObjectInputStream objectOutputStream = null;
            objectOutputStream = new ObjectInputStream(fileInputStream);
            families = (ArrayList<Family>) objectOutputStream.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


        return families;
    }
    public void loadData(List<Family> families){
        FileOutputStream fileOutputStream = null;
        try {
            fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = null;
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(families);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
