package com.danit;

import java.util.Set;

public class RoboCat extends Pet implements PetFoul {
    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(Species.ROBO_CAT, nickname, age, trickLevel, habits);
    }


    @Override
    public void respond() {

    }
    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
}
