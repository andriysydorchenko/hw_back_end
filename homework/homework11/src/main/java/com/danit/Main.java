package com.danit;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.HashSet;
import java.util.Set;

public class Main{
    public static void main(String[] args) throws CloneNotSupportedException {
        Console console = new Console();
        console.run();

    }

}