package com.danit;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Console {
    private Map<Integer, String> mainMenu = new HashMap<>();
    FamilyController familyController = new FamilyController();

    public void run() {
        while (true) {
            fillMainMenuOptions();
            showMainMenu();
            handleChoosedMainMenuOption();
        }
    }

    private void fillMainMenuOptions() {
        mainMenu.put(1, "Заполнить тестовыми данными");
        mainMenu.put(2, "Отобразить весь список семей");
        mainMenu.put(3, "Отобразить список семей, где количество людей больше заданного");
        mainMenu.put(4, "Отобразить список семей, где количество людей меньше заданного");
        mainMenu.put(5, "Подсчитать количество семей, где количество членов равно");
        mainMenu.put(6, "Создать новую семью");
        mainMenu.put(7, "Удалить семью по индексу семьи в общем списке");
        mainMenu.put(8, "Редактировать семью по индексу семьи в общем списке");
        mainMenu.put(9, "Удалить всех детей старше возраста");
    }
    private void showMainMenu() {
        System.out.println("Выберите пункт меню, который Вас интересует:");
        for (Map.Entry<Integer, String> entry: mainMenu.entrySet()) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
    }

    private void handleChoosedMainMenuOption(){
        int option = readInt("Выберите пункт меню, который Вас интересует:", 1, 10);
        switch (option) {
            case 1:
                putTestData();
                break;
            case 2:
                showFamilies();
                break;
            case 3:
                showFamiliesBiggerThan();
                break;
            case 4:
                showFamiliesLessThan();
                break;
            case 5:
                showFamiliesWithMemberNumber();
                break;
            case 6:
                createNewFamily();
                break;
            case 7:
                deleteFamilyByIndex();
                break;
            case 8:
                editFamilyByIndex();
                break;

            case 9:
                deleteAllChildrenOlderThen();
                break;
            case 10:
                exit();
                break;
        }
    }
    private void exit() {
        System.exit(1);
    }
    private void putTestData () {
        try {
            UtilsCreateTestFamily.createTestFamily(familyController);
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
    private void showFamilies (){
        List<Family> allFamilies = familyController.getAllFamilies();
        printFamilies(allFamilies);
    }
    public void printFamilies(List<Family> familyList){
        for (int i = 0, allFamiliesSize = familyList.size(); i < allFamiliesSize; i++) {
            Family family = familyList.get(i);
            System.out.println("Family index: " + i + "\n" + family.prettyFormat());
        }
    }
    public void showFamiliesBiggerThan() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите заданное количество людей:");
        int count = scanner.nextInt();
        List<Family> familiesBiggerThan = familyController.getFamiliesBiggerThan(count);
        printFamilies(familiesBiggerThan);
    }
    public void showFamiliesLessThan() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите заданное количество людей:");
        int count = scanner.nextInt();
        List<Family> familiesLessThan = familyController.getFamiliesLessThan(count);
        printFamilies(familiesLessThan);
    }
    public void showFamiliesWithMemberNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите заданное количество людей:");
        int count = scanner.nextInt();
        int i = familyController.countFamiliesWithMemberNumber(count);
        System.out.println("Количество семей с заданым числом: " + i);
    }
    public void createNewFamily(){
        String motherName = readStr("Введите имя матери:");
        String motherSurname = readStr("Введите фамилию матери:");
        int yearMother = readInt("Введите год рождения матери:", 1900, LocalDate.now().getYear());
        int monthMother = readInt("Введите месяц рождения матери:", 1, 12);
        int dayMother = readInt("Введите день рождения матери:", 1, 31);
        int iqMother = readInt("Введите iq матери:", 1, 200);
        LocalDate birthDateMother = LocalDate.of(yearMother, monthMother, dayMother);
        long birthDateMilMother = birthDateMother.atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
        Woman woman = new Woman(motherName, motherSurname, birthDateMilMother, iqMother);

        String fatherName = readStr("Введите имя отца:");;
        String fatherSurname = readStr("Введите фамилию отца:");;;
        int yearFather = readInt("Введите год рождения отца:", 1900, LocalDate.now().getYear());
        int monthFather = readInt("Введите месяц рождения отца:", 1, 12);
        int dayFather = readInt("Введите день рождения отца:", 1, 31);
        int iqFather = readInt("Введите iq отца:", 1, 200);
        LocalDate birthDateFather = LocalDate.of(yearFather, monthFather, dayFather);
        long birthDateMilFather = birthDateFather.atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
        Human man = new Man(fatherName, fatherSurname, birthDateMilFather, iqFather);

        familyController.createNewFamily(woman,man);

    }
    public void deleteFamilyByIndex() {
        int index = readInt("Введите индекс семьи", 0, 10000);
        familyController.deleteFamilyByIndex(index);
    }
    public void editFamilyByIndex() {
        String str = """
                        1. Родить ребенка
                        2. Усыновить ребенка
                        3. Вернуться в главное меню
                        Выберете как редактировать
                     """;
        int commandIndex = readInt(str, 1, 3);
        if(commandIndex == 1) {
            int index = readInt("Введите индекс семьи", -1, familyController.count());
            String nameBoy = readStr("Введите имя мальчика");
            String nameGirl= readStr("Введите имя девочки");
            Family familyByIndex = familyController.getFamilyByIndex(index);
            try {
                familyController.bornChild(familyByIndex, nameBoy, nameGirl);
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        } else if (commandIndex == 2) {
            int index = readInt("Введите индекс семьи", 0, familyController.count());
            String name = readStr("Введите имя матери:");
            String surname = readStr("Введите фамилию матери:");
            int year = readInt("Введите год рождения матери:", 1900, LocalDate.now().getYear());
            int month= readInt("Введите месяц рождения матери:", 1, 12);
            int day = readInt("Введите день рождения матери:", 1, 31);
            int iq = readInt("Введите iq матери:", 1, 200);
            LocalDate birthDate = LocalDate.of(year, month, day);
            long birthDateMil= birthDate.atStartOfDay(ZoneOffset.UTC).toInstant().toEpochMilli();
            Human child = new Human(name, surname, birthDateMil, iq);
            Family familyByIndex = familyController.getFamilyByIndex(index);
            try {
                familyController.adoptChild(familyByIndex, child);
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
    }
    public void deleteAllChildrenOlderThen(){
        int age =  readInt("Введите инетресующий возраст", 1, 200);
        familyController.deleteAllChildrenOlderThen(age);
    }
    public int readInt(String prompt, int min, int max){
        Scanner scan = new Scanner(System.in);
        int number = 0;
        do
        {
            System.out.printf("\n%s > ", prompt);
            while (!scan.hasNextInt())
            {
                System.out.printf("Input doesn't match specifications. Try again.");
                System.out.printf("\n%s > ", prompt);
                scan.next();
            }
            number = scan.nextInt();
            if (number < min || number > max)
                System.out.printf("Input doesn't match specifications. Try again.");

        } while (number < min || number > max);
        return number;
    }
    public String readStr(String prompt){
        Scanner scan = new Scanner(System.in);
        String str;
        do
        {
            System.out.printf("\n%s > ", prompt);
            while (!scan.hasNextLine())
            {
                System.out.printf("Input doesn't match specifications. Try again.");
                System.out.printf("\n%s > ", prompt);
                scan.next();
            }
            str = scan.nextLine();
            if (str.length() == 0)
                System.out.printf("Нужно что-то ввести6");
        } while (str.length() == 0);
        return str;
    }
}
