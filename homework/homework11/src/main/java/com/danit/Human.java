package com.danit;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Locale;

public class Human {
    private String name;
    private String surname;
    private long birthDate;
    private int iq;
    private Pet pet;
    private Family family;
    private Human mother;
    private Human father;
    private HashMap<String, String> schedule = new HashMap<String, String>();

    //constructors
    public Human(){
    }
    public Human(String name, String surname, long birthDate){
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
    }

    public Human(String name, String surname, long birthDate, int iq) {
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
    }

    public Human(String name, String surname, long birthDate, Human mother, Human father){
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.mother = mother;
        this.father = father;
    }
    public Human(String name, String surname, long birthDate, int iq, Human mother, Human father, Pet pet, HashMap<String, String> schedule, Family family){
        this.name = name;
        this.surname = surname;
        this.birthDate = birthDate;
        this.iq = iq;
        this.mother = mother;
        this.father = father;
        this.pet = pet;
        this.schedule  = schedule;
        this.family = family;
    }

    public void greetPet() {
        System.out.printf("Привет, %s" , pet.getNickname());
        System.out.println();
    }
    public void describePet() {
        String s = String.format("У меня есть %s, ему %d лет, он %s", pet.getSpecies(), pet.getAge(), pet.getTrickLevel());
        System.out.println(s);
    }
    public String describeAge(){
        LocalDate birthDayDate = Instant
                .ofEpochMilli(birthDate)
                .atOffset(
                        ZoneOffset.UTC
                )
                .toLocalDate();
        return birthDayDate.toString();

    }
    public String prettyFormat(){
        String str = """
                        name='%s', surname='%s', birthDate='%s', iq='%s', schedule=%s""".formatted(name, surname, birthDate, iq, schedule);
        return str;
    }



    @Override
    public String toString() {
        String nameStr  = name == null ? "no name" : name;
        String surnameStr  = surname == null ? "no surname" : surname;
        String motherStr  = mother == null ? "no mother" : mother.getFullName();
        String fatherStr  = father == null ? "no father" : father.getFullName();
        String petString = pet == null ? "no pets" : pet.toString();
        LocalDate birthDayDate = Instant
                .ofEpochMilli(birthDate)
                .atOffset(
                        ZoneOffset.UTC
                )
                .toLocalDate();
        String birthDay = birthDayDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
        return "Human{" +
                "name='" + nameStr + '\'' +
                ", surname='" + surnameStr + '\'' +
                ", birthDate=" + birthDay +
                ", iq=" + iq +
                ", pet=" + petString +
                ", mother=" + motherStr +
                ", father=" + fatherStr +
                ", schedule=" + schedule +
                '}';
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human that = (Human) o;

        if (!name.equals(that.name)) return false;
        if (!surname.equals(that.surname)) return false;
        if (birthDate != that.birthDate) return false;
        if (iq != that.iq) return false;
        if (mother != that.mother) return false;
        return father == that.father;
    }
    @Override
    public int hashCode() {
        int result = name == null ? 0 : name.hashCode();
        result = 31 * result + (surname == null ? 0 : surname.hashCode());
        result = (int) (31 * result + birthDate);
        result = 31 * result + iq;
        result = 31 * result + (mother == null ? 0 : mother.hashCode());
        result = 31 * result + (father == null ? 0 : father.hashCode());
        return result;
    }
    @Override
    protected void finalize(){
        System.out.println("deleted exemplar of class Human");
    }
    //    getters

    public String getFullName() {
        return name + " " + surname;
    }
    public Human getFather() {
        return father;
    }
    public Human getMother() {
        return mother;
    }
    public Family getFamily() {
        return family;
    }
    public int getIq() {
        return iq;
    }
    public long getYear() {
        return birthDate;
    }
    public Pet getPet() {
        return pet;
    }
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public HashMap<String, String> getSchedule() {
        return schedule;
    }

    //    setter
    public void setName(String name){
        this.name = name;
    }
    public void setSurname(String surname){this.surname = surname;}
    public void setYear(long birthDate){ this.birthDate = birthDate;}
    public void setIq(int iq){this.iq = iq;}
    public void setPet(Pet pet){this.pet = pet;}
    public void setFamily(Family family){
        this.family = family;
    }
    public void setMother(Human mother){this.mother = mother;}
    public void setFather(Human father){this.father = father;}
    public void setSchedule(HashMap<String, String> schedule) {
        this.schedule = schedule;
    }
}

