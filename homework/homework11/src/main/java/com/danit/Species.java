package com.danit;

public enum Species {
    DOG,
    ROBO_CAT,
    DOMESTIC_CAT,
    FISH,
    UNKNOWN
}
