package com.danit;
import java.util.HashSet;
import java.util.Set;


public abstract class Pet {
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits = new HashSet();

    public Pet() {

    }
    public Pet(String nickname) {
        this.species = species;
        this.nickname = nickname;
    }
    public Pet(Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void eat() {
        System.out.println("Я кушаю!");
    }
    public abstract void respond();
    //    getters
    public int getAge() {
        return age;
    }
    public Species getSpecies() {
        return species;
    }
    public String getTrickLevel() {
        if (trickLevel > 50) {
            return "очень хитры";
        }
        return "почти не хитрый";
    }
    public String getNickname() {
        return nickname;
    }
    public Set<String>  getHabits() {
        return habits;
    }
    public String prettyFormat(){
        String str = """
                        %s: nickname= %s, age= %s, trickLevel= %s, habits= %s """.formatted(species, nickname, age, trickLevel, habits);
        return str;
    }

    //    setters
    public void setAge(int age) {
        this.age = age;
    }
    public void setHabits(String  habits) {
        this.habits.add(habits);
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public void setSpecies(Species species) {
        this.species = species;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    @Override
    public String toString() {
        return "{" +
                "species=" + species +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pet that = (Pet) o;

        if (!species.equals(that.species)) return false;
        if (!nickname.equals(that.nickname)) return false;
        return trickLevel == that.trickLevel;
    }
    @Override
    public int hashCode() {
        int result = species == null ? 0 : species.hashCode();
        result = 31 * result + (nickname == null ? 0 : nickname.hashCode());
        result = 31 * result + trickLevel;
        return result;
    }
    @Override
    protected void finalize() {
        System.out.println("deleted exemplar of class Pet");
    }
}
