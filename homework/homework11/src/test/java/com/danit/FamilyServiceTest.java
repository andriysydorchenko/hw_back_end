package com.danit;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class FamilyServiceTest {
    Set<String> habits = new HashSet<>();
    Pet roboCat1 = new RoboCat("roboCat1", 2, 80, habits);
    Pet dog1 = new Dog("dog1", 5, 15, habits);
    Pet dog2 = new Dog("dog2", 2, 90, habits);

    Human mother1 = new Human("mother1", "surname", 56, null, null);
    Human father1 = new Human("father1", "surname", 56, null, null);

    Woman woman1 = new Woman("nameMother1", "surnameMother1", 30, 160, mother1, father1, roboCat1, null, null);
    Human man1 = new Man("nameFather1", "surnameFather1", 30, 160, mother1, father1, dog1, null, null);
    Human woman2 = new Woman("nameMother2", "surnameMother2", 30, 160, mother1, father1, null, null, null);
    Human man2 = new Man("nameFather2", "surnameFather2", 30, 160, mother1, father1, dog1, null, null);

    Human child1 = new Woman("nameChild1", "surnameChild1", 20, 180, mother1, father1, null, null, null);
    Human child2 = new Man("nameChild2", "surnameChild2", 15, 120, mother1, father1, null, null, null);

    Family family1 = new Family(woman1, man1);
    Family family2 = new Family(woman2, man2);
    List<Family> familyList = new ArrayList<>();

    @Test
    public void should_getAllFamily_when_hasFamily() {
        //given
        familyList.add(family1);
        familyList.add(family2);
        //when
        CollectionFamilyDao collectionFamilyDao = mock(CollectionFamilyDao.class);
        when(collectionFamilyDao.getAllFamilies()).thenReturn(familyList);

        FamilyService familyService = new FamilyService(collectionFamilyDao);
        List<Family> allFamilies = familyService.getAllFamilies();
        //then

        assertEquals(allFamilies, familyList);
        assertArrayEquals(allFamilies.toArray(), familyList.toArray());
    }

    @Test
    public void should_getFamilyBiggerThan_when_hasThanFamily(){
        //given
        familyList.add(family1);
        int countMembers = 3;

        //when
        CollectionFamilyDao collectionFamilyDao = mock(CollectionFamilyDao.class);
        when(collectionFamilyDao.getFilteredFamilies(e -> e.getCountFamily() > countMembers)).thenReturn(familyList);

        FamilyService familyService = new FamilyService(collectionFamilyDao);
        List<Family> familiesBiggerThan = familyService.getFamiliesBiggerThan(countMembers);
        //then

        assertArrayEquals(familyList.toArray(), familiesBiggerThan.toArray());
    }
    @Test
    public void should_getFamiliesLessThan_when_hasThanFamily(){
        //given
        familyList.add(family1);
        int countMembers = 3;

        //when
        CollectionFamilyDao collectionFamilyDao = mock(CollectionFamilyDao.class);
        when(collectionFamilyDao.getFilteredFamilies(e -> e.getCountFamily() < countMembers)).thenReturn(familyList);

        FamilyService familyService = new FamilyService(collectionFamilyDao);
        List<Family> familiesBiggerThan = familyService.getFamiliesBiggerThan(countMembers);
        //then

        assertArrayEquals(familyList.toArray(), familiesBiggerThan.toArray());
    }
    @Test
    public void should_countFamiliesWithMemberNumber_when_hasThanFamily(){
        familyList.add(family1);
        int countMembers = 2;

        //when
        CollectionFamilyDao collectionFamilyDao = mock(CollectionFamilyDao.class);
        when(collectionFamilyDao.getFilteredFamilies(e -> e.getCountFamily() == countMembers)).thenReturn(familyList);

        FamilyService familyService = new FamilyService(collectionFamilyDao);
        List<Family> familiesBiggerThan = familyService.getFamiliesBiggerThan(countMembers);
        //then

        assertArrayEquals(familyList.toArray(), familiesBiggerThan.toArray());
    }

    @Test
    public void should_deleteFamilyByIndex_when_Deleted(){
        //given
        familyList.add(family1);
        //when
        CollectionFamilyDao collectionFamilyDao = mock(CollectionFamilyDao.class);
        when(collectionFamilyDao.deleteFamily(1)).thenReturn(true);

        FamilyService familyService = new FamilyService(collectionFamilyDao);
        boolean result= familyService.deleteFamilyByIndex(1);
        //then
        assertEquals(true, result);
    }

    @Test
    public void should_bornChild_when_countFamilyBigger() throws CloneNotSupportedException {
        //given
        familyList.add(family1);
        //when
        CollectionFamilyDao collectionFamilyDao = mock(CollectionFamilyDao.class);
        doNothing().when(collectionFamilyDao).saveFamily(family1);

        FamilyService familyService = new FamilyService(collectionFamilyDao);
        Family newFamily = familyService.bornChild(family1, "man", "woman");
        //then
        assertEquals(newFamily.getCountFamily(), 3);
    }
    @Test
    public void should_adoptChild_when_countFamilyBigger() throws CloneNotSupportedException {
        //given
        familyList.add(family1);
        //when
        CollectionFamilyDao collectionFamilyDao = mock(CollectionFamilyDao.class);
        doNothing().when(collectionFamilyDao).saveFamily(family1);

        FamilyService familyService = new FamilyService(collectionFamilyDao);
        Family newFamily = familyService.adoptChild(family1, child1);
        //then
        assertEquals(newFamily.getCountFamily(), 3);
        assertEquals(newFamily.getChildren().get(0), child1);
    }
    @Test
    public void should_deleteAllChildrenOlderThen_when_Deleted(){
        //given
        family1.addChild(child1);
        family1.addChild(child2);
        Family resultFamily = new Family(woman1,man1);
        resultFamily.addChild(child2);
        familyList.add(resultFamily);
        //when
        CollectionFamilyDao collectionFamilyDao = mock(CollectionFamilyDao.class);
        doNothing().when(collectionFamilyDao).saveFamily(family1);
        when(collectionFamilyDao.getAllFamilies()).thenReturn(familyList);

        FamilyService familyService = new FamilyService(collectionFamilyDao);
        familyService.deleteAllChildrenOlderThen(16);
        List<Family> allFamilies = familyService.getAllFamilies();
        //then
        assertArrayEquals(familyList.toArray(), allFamilies.toArray());
    }
    @Test
    public void should_getCount_when_rightCount(){
        //given
        familyList.add(family1);
        familyList.add(family2);
        //when
        CollectionFamilyDao collectionFamilyDao = mock(CollectionFamilyDao.class);
        when(collectionFamilyDao.getAllFamilies()).thenReturn(familyList);

        FamilyService familyService = new FamilyService(collectionFamilyDao);
        int count = familyService.count();
        //then
        assertEquals(count, familyList.size());
    }
    @Test
    public void should_getFamilyByIndex_when_findFamily(){
        //given
        familyList.add(family1);
        familyList.add(family2);
        //when
        CollectionFamilyDao collectionFamilyDao = mock(CollectionFamilyDao.class);
        when(collectionFamilyDao.getFamilyByIndex(1)).thenReturn(family2);
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        Family familyByIndex = familyService.getFamilyByIndex(1);
        //then
        assertEquals(familyByIndex, family2);
    }
    @Test
    public void should_getPets_when_getPets(){
        //given
        family2.addPet(dog1);
        family2.addPet(dog2);
        Set<Pet> resultPet = family2.getPet();
        //when
        CollectionFamilyDao collectionFamilyDao = mock(CollectionFamilyDao.class);
        when(collectionFamilyDao.getFamilyByIndex(1)).thenReturn(family2);
        FamilyService familyService = new FamilyService(collectionFamilyDao);
        Set<Pet> pets = familyService.getPets(1);
        //then
        assertArrayEquals(resultPet.toArray(), pets.toArray());
    }
}