package App;

import java.util.Arrays;

class Pet{
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet() {

    }
    public Pet(String species, String nickname){
        this.species = species;
        this.nickname = nickname;
    }
    public Pet (String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void eat(){
        System.out.println("Я кушаю!");
    }
    public void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!", nickname);
        System.out.println();
    }
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
//    getters
    public int getAge() {
        return age;
    }
    public String getSpecies() {
        return species;
    }
    public String getTrickLevel() {
        if(trickLevel > 50) {
            return "очень хитры";
        }
        return "почти не хитрый";
    }
    public String getNickname() {
        return nickname;
    }
    public String[] getHabits() {
        return habits;
    }
//    setters
    public void setAge(int age) {
        this.age = age;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public void setSpecies(String species) {
        this.species = species;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    @Override
    public String toString(){
        String s = String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}", species, nickname, age, trickLevel, Arrays.toString(habits));
        return s;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pet that = (Pet) o;

        if (!species.equals(that.species)) return false;
        if (!nickname.equals(that.nickname)) return false;
        return trickLevel == that.trickLevel;
    }
    @Override
    public int hashCode() {
        int result = species == null ? 0 : species.hashCode();
        result = 31 * result + (nickname == null ? 0 : nickname.hashCode());
        result = 31 * result + trickLevel;
        return result;
    }
}

class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Family family;
    private Human mother;
    private Human father;
    private String[][] schedule = new String[7][7];

    public Human(){

    }
    public Human(String name, String surname, int year){
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name, String surname, int year,  Human mother, Human father){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }
    public Human(String name, String surname, int year, Human mother, Human father, int iq, Pet pet, String[][] schedule, Family family){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
        this.iq = iq;
        this.pet = pet;
        this.schedule  = schedule;
        this.family = family;
    }

    public void greetPet() {
        System.out.printf("Привет, %s" , pet.getNickname());
        System.out.println();
    }
    public  void describePet() {
        String s = String.format("У меня есть %s, ему %d лет, он %s", pet.getSpecies(), pet.getAge(), pet.getTrickLevel());
        System.out.println(s);
    }
    @Override
    public String toString(){
        String motherStr  = mother == null ? "no mother" : mother.getFullName();
        String fatherStr  = father == null ? "no father" : father.getFullName();
        String petString = pet == null ? " no pets" : pet.toString();

        String s = String.format("Human{name='%s', surname='%s', year=%d, iq=%d, mother=%s, father=%s, pet=%s}", name, surname, year, iq, motherStr, fatherStr, petString);
        return s;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Human that = (Human) o;

        if (!name.equals(that.name)) return false;
        if (!surname.equals(that.surname)) return false;
        if (year != that.year) return false;
        if (iq != that.iq) return false;
        if (mother != that.mother) return false;
        return father == that.father;
    }
    @Override
    public int hashCode() {
        int result = name == null ? 0 : name.hashCode();
        result = 31 * result + (surname == null ? 0 : surname.hashCode());
        result = 31 * result + year;
        result = 31 * result + iq;
        result = 31 * result + (mother == null ? 0 : mother.hashCode());
        result = 31 * result + (father == null ? 0 : father.hashCode());
        return result;
    }
//    getters
    public String getFullName() {
        return name + " " + surname;
    }
    public Human getFather() {
        return father;
    }
    public Human getMother() {
        return mother;
    }
    public Family getFamily() {
        return family;
    }
    public int getIq() {
        return iq;
    }
    public int getYear() {
        return year;
    }
    public Pet getPet() {
        return pet;
    }
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
//    setter
    public void setName(String name){
        this.name = name;
    }
    public void setSurname(String surname){this.surname = surname;}
    public void setYear(int year){ this.year = year;}
    public void setIq(int iq){this.iq = iq;}
    public void setPet(Pet pet){this.pet = pet;}
    public void setFamily(Family family){
        this.family = family;
    }
    public void setMother(Human mother){this.mother = mother;}
    public void setFather(Human father){this.father = father;}
    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }
}

class Family {
    private Human mother;
    private Human father;
    private Human[] children = new Human[0];
    private Pet pet;
    private int point = 0;
    public Family(Human mother, Human father){
        this.mother = mother;
        this.father = father;
    }
//    getters

    public Human getMother() {
        return mother;
    }
    public Human getFather() {
        return father;
    }
    public Human[] getChildren() {
        return children;
    }
    public Pet getPet() {
        return pet;
    }

    //    setters
    public void setChildren(Human[] children) {
        this.children = children;
    }
    public void setFather(Human father) {
        this.father = father;
    }
    public void setMother(Human mother) {
        this.mother = mother;
    }
    public void setPet(Pet pet) {
        this.pet = pet;
        mother.setPet(pet);
        father.setPet(pet);
        for (int i = 0; i < children.length; i++) {
            children[i].setPet(pet);
        }
    }

    public void addChild(Human child) {
        Human[] temp = children;
        children = new Human[temp.length + 1];
        System.arraycopy(temp, 0, children, 0, point);
        children[point] = child;
        point++;
        child.setFamily(this);
    }
    public boolean deleteChild(int indexChild) {
        if(children.length > indexChild){
            Human[] temp = children;
            children = new Human[temp.length - 1];
            System.arraycopy(temp, 0, children, 0, indexChild+1);
            System.arraycopy(temp, indexChild+1, children, indexChild, temp.length+1-indexChild+1);
            point--;
            children[indexChild].setFamily(null);
            return true;
        } else {
            return false;
        }
    }
    public int getCountFamily(){
        return 2+children.length;
    }
    @Override
    public String toString(){
        String str = """
                        father: %s
                        mother: %s
                        children: %s
                     """.formatted(father.toString(), mother.toString(), Arrays.toString(children));
        return str;
    }

}
class Main{
    public static void main(String[] args) {
        Human father1 = new Human();
        father1.setName("Father1");
        father1.setSurname("SurFather1");
        Human mother1 = new Human();
        mother1.setName("Mother1");
        mother1.setSurname("SurMother1");
        Human child1 = new Human();
        Pet pet1 = new Pet();
        Family family1 = new Family(mother1,father1);
        family1.addChild(child1);
        family1.setPet(pet1);

        Human father2 = new Human("Father2", "SurFather2", 1990);
        Human mother2 = new Human("Mother2", "SurMother2", 1990);
        Human child2 = new Human("Child2", "SurChild2", 2005);
        Pet pet2 = new Pet("cat", "Garfield");
        Family family2 = new Family(mother2,father2);
        family2.addChild(child2);
        family2.setPet(pet2);

        String[][] schedule = {{"1", "2", "3", "4", "5", "6", "7"}, {"do1", "do2", "do3", "do4", "do5", "do6", "do7"}};
        Human father3 = new Human("Father2", "SurFather2", 1990, mother1, father1,  65, null, schedule, null);
        Human father4 = new Human("Father2", "SurFather2", 1990, mother1, father1,  65, null, schedule, null);
        Human mother3 = new Human("Mother2", "SurMother2", 1990, mother2, father2,  74, null, schedule, null);
        Human child3 = new Human("Child2", "SurChild2", 2005, mother3, father3,  70, null, schedule, null);
        Pet pet3 = new Pet("dog", "Belka", 3, 60, new String[]{"to more eat", "gav, gav"});
        Family family3 = new Family(mother3,father3);
        family3.addChild(child3);
        family3.setPet(pet3);

        System.out.println(family3.toString());
        child3.greetPet();
        child3.describePet();

        pet3.eat();
        pet3.foul();
        pet3.respond();

        System.out.println(father3.equals(father4));
        System.out.println(father3.hashCode() == father4.hashCode());
    }
}