package com.danit;

public interface HumanCreator {
    Family bornChild(Family family, String manName, String womanName) throws CloneNotSupportedException;
}
