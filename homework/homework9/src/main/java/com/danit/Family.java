package com.danit;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class Family implements Cloneable{
    private Human mother;
    private Human father;
    private List<Human> children = new ArrayList<>();
    private Set<Pet> pets = new HashSet();
    private int point = 0;

    //    constructors
    public Family(Human mother, Human father){
        this.mother = mother;
        if(this.mother.getPet() != null){
            pets.add(this.mother.getPet());
        }
        mother.setFamily(this);
        this.father = father;
        if(this.father.getPet() != null){
            pets.add(this.father.getPet());
        }
        father.setFamily(this);
    }

    //    getters
    public Human getMother() {
        return mother;
    }
    public Human getFather() {
        return father;
    }
    public List<Human>  getChildren() {
        return children;
    }
    public Set<Pet> getPet() {
        return pets;
    }

    //    setters
    public void setChildren(List<Human>  children) {
        this.children = children;
    }
    public void setFather(Human father) {
        this.father = father;
    }
    public void setMother(Human mother) {
        this.mother = mother;
    }
    public void setPet(Pet pet) {
        this.pets.add(pet);
        mother.setPet(pet);
        father.setPet(pet);
        for (Human child: children) {
            child.setPet(pet);
        }
    }

    //other methods
    public void addChild(Human child) {
        children.add(child);
        if(child.getPet() != null){
            pets.add(child.getPet());
        }
        child.setFamily(this);
    }
    public void addPet (Pet pet){
        pets.add(pet);
    }
    public boolean deleteChild(int indexChild) {
        try{
            children.remove(indexChild);
            return true;
        }catch (IndexOutOfBoundsException e){
            System.out.println(e.getMessage());
            return false;
        }
    }
    public boolean deleteChild(Human child) {
        return children.remove(child);
    }
    public int getCountFamily(){
        return 2+children.size();
    }

    @Override
    public String toString(){
        String str = """
                        father: %s
                        mother: %s
                        children: %s
                     """.formatted(father.toString(), mother.toString(), children.toString());
        return str;
    }
    @Override
    protected void finalize(){
        System.out.println("deleted exemplar of class Family");
    }
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Family)) return false;

        Family family = (Family) o;

        if (getMother() != null ? !getMother().equals(family.getMother()) : family.getMother() != null) return false;
        return getFather() != null ? getFather().equals(family.getFather()) : family.getFather() == null;
    }
    @Override
    public int hashCode() {
        int result = getMother() != null ? getMother().hashCode() : 0;
        result = 31 * result + (getFather() != null ? getFather().hashCode() : 0);
        return result;
    }

}
