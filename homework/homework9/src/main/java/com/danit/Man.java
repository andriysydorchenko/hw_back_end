package com.danit;

import java.util.HashMap;

public final class Man extends Human {
    public Man(String name, String surname, String birthDate, int iq) {
        super(name, surname, birthDate, iq);
    }

    public Man(String name, String surname, long birthDate, int iq, Human mother, Human father, Pet pet, HashMap<String, String> schedule, Family family) {
        super(name, surname, birthDate, iq, mother, father, pet, schedule, family);
    }

    @Override
    public void greetPet() {
        System.out.printf("Hay, %s" , this.getPet().getNickname());
        System.out.println();
    }
    public void repairCar(){
        System.out.println("Car need to repair");
    }
}
