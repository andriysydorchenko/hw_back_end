package com.danit.app;

public final class Man extends Human{
    @Override
    public void greetPet() {
        System.out.printf("Hay, %s" , this.getPet().getNickname());
        System.out.println();
    }
    public void repairCar(){
        System.out.println("Car need to repair");
    }
}
