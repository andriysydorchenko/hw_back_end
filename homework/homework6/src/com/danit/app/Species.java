package com.danit.app;

public enum Species {
    DOG,
    ROBO_CAT,
    DOMESTIC_CAT,
    FISH,
    UNKNOWN
}
