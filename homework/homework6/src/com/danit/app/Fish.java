package com.danit.app;

public class Fish extends Pet{
    private Species species = Species.FISH;
    @Override
    public void respond() {

    }
    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }
}
