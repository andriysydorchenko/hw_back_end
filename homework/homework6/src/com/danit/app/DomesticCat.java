package com.danit.app;

public class DomesticCat extends Pet implements PetFoul{
    private Species species = Species.DOMESTIC_CAT;
    @Override
    public void respond() {

    }
    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }

    @Override
    public Species getSpecies() {
        return species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }
}
