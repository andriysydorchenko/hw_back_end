package game;

import java.util.Random;
import java.util.Scanner;

public class MatrixGame {
    public static void main(String[] args) {
        String[][] matrix= new String[6][6];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                if( i == 0 ){
                    matrix[i][j] = "" + j;
                } else if (j == 0) {
                    matrix[i][j] = "" + i;
                } else {
                    matrix[i][j] = "-";
                }
            }
        }
        startGame(matrix);

    }
    public static void startGame (String[][] matrix){
        Random random = new Random();
        int randomLine = random.nextInt(matrix.length-3)+1;
        int randomColumn = random.nextInt(matrix.length-3)+1;
        System.out.println("All set. Get ready to rumble!");
        boolean flag = true;
        while (flag) {
            System.out.println("Enter the line");
            int lineNumber = readIntFromScanner(1, 5);
            System.out.println("Enter the column");
            int lineColumn = readIntFromScanner(1, 5);

            if (lineColumn == randomColumn && lineNumber == randomLine){
                matrix[lineNumber][lineColumn] = "x";
                flag =false;
                System.out.println("You have won!");
            }else{
                matrix[lineNumber][lineColumn] = "*";
            }
            printMatrix(matrix);
        }
    }
    public static int readIntFromScanner(int min, int max){
        Scanner scanner = new Scanner(System.in);
        while (true){
            if (scanner.hasNextInt()){
                int number = scanner.nextInt();
                if (number >= min && number <= max){
                    return number;
                }
                else{
                    System.out.printf("min = %s, max = %s please!", min, max);
                    scanner.nextLine();
                }
            }
            else{
                System.out.println("int please!");
                scanner.nextLine();
            }
        }
    }
    public static void printMatrix (String[][] matrix){
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                System.out.print(matrix[i][j]+" | ");
            }
            System.out.println();
        }
    }
}