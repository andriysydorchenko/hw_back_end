package game;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.Scanner;

public class App{
    public static void main(String[] args) {
        System.out.println("Let the game begin!");
        Random random = new Random();
        int number = random.nextInt(101);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter you name");
        String name =scanner.nextLine();
        System.out.println(name + " " + number);
        int[] cashNumbers = new int[100];
        int i = 0;
        boolean flag = true;
        while (flag) {
            System.out.println("Enter you number");
            int[] userNumber = new int[i+1];
            try {
                userNumber[i] = scanner.nextInt();
                cashNumbers[i] = userNumber[i];
            } catch(NoSuchElementException | NullPointerException e) {
                System.out.println("Enter the only number");
                scanner.nextLine();
                continue;
            }
            if(userNumber[i] == number){
                System.out.printf("Congratulations, %s!", name);
                System.arraycopy(cashNumbers, 0, userNumber, 0, i);
                Arrays.sort(userNumber);
                System.out.println("Your numbers:" + Arrays.toString(userNumber));
                flag = false;
            } else if ( userNumber[i] < number) {
                System.out.println("Your number is too small. Please, try again.");
                i++;
            } else if (userNumber[i] > number){
                System.out.println("Your number is too big. Please, try again.");
                i++;
            }
        }
    }
}