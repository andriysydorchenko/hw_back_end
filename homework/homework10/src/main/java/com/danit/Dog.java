package com.danit;

import java.util.Set;

public class Dog extends Pet implements PetFoul {
    public Dog(String nickname, int age, int trickLevel, Set<String> habits) {
        super(Species.DOG, nickname, age,trickLevel, habits);
    }
    @Override
    public void respond() {

    }
    @Override
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
}
