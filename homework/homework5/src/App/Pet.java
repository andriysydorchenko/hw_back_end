package App;
import java.util.Arrays;

public class Pet{
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet() {

    }
    public Pet(Species species, String nickname){
        this.species = species;
        this.nickname = nickname;
    }
    public Pet (Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public void eat(){
        System.out.println("Я кушаю!");
    }
    public void respond() {
        System.out.printf("Привет, хозяин. Я - %s. Я соскучился!", nickname);
        System.out.println();
    }
    public void foul() {
        System.out.println("Нужно хорошо замести следы...");
    }
    //    getters
    public int getAge() {
        return age;
    }
    public Species getSpecies() {
        return species;
    }
    public String getTrickLevel() {
        if(trickLevel > 50) {
            return "очень хитры";
        }
        return "почти не хитрый";
    }
    public String getNickname() {
        return nickname;
    }
    public String[] getHabits() {
        return habits;
    }
    //    setters
    public void setAge(int age) {
        this.age = age;
    }
    public void setHabits(String[] habits) {
        this.habits = habits;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }
    public void setSpecies(Species species) {
        this.species = species;
    }
    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    @Override
    public String toString(){
        String s = String.format("%s{nickname='%s', age=%d, trickLevel=%d, habits=%s}", species, nickname, age, trickLevel, Arrays.toString(habits));
        return s;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pet that = (Pet) o;

        if (!species.equals(that.species)) return false;
        if (!nickname.equals(that.nickname)) return false;
        return trickLevel == that.trickLevel;
    }
    @Override
    public int hashCode() {
        int result = species == null ? 0 : species.hashCode();
        result = 31 * result + (nickname == null ? 0 : nickname.hashCode());
        result = 31 * result + trickLevel;
        return result;
    }
    @Override
    protected void finalize(){
        System.out.println("deleted exemplar of class Pet");
    }
}


enum Species {
    DOG,
    CAT,
    DUCK,
    BIRD,
    RACCOON
}