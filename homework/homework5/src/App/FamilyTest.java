package App;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FamilyTest {
    private static Human father = new Human("Father2", "SurFather2", 1990);
    private static Human mother = new Human("Mother2", "SurMother2", 1990);
    private static Human child1 = new Human("name child1", "surname child1", 2020);
    private static Human child2 = new Human("name child2", "surname child2", 2020);


    @Test
    public void should_addChild_when_childAdded(){
        //given
        Family family = new Family(father, mother);
        family.addChild(child1);
        //when
        Human result = family.getChildren()[0];
        //then
        assertEquals(result, child1);
        assertEquals(family, child1.getFamily());
    }
    @Test
    public void should_getFamily_when_returnValidCountFamily(){
        //given
        Family family = new Family(father, mother);
        family.addChild(child1);
        //when
        int result = family.getCountFamily();
        //then
        assertEquals(result, 3);
    }


    @Test
    public void should_getNonValidIndex_when_notFoundChild(){
        //given
        int index = 5;
        Family family = new Family(father, mother);
        family.addChild(child1);
        family.addChild(child2);
        //when
        boolean deleted = family.deleteChild(index);
        Human[] result = family.getChildren();
        //then
        Human[] expected = new Human[] {child1, child2};
        assertEquals(deleted, false);
        assertArrayEquals(result, expected);

    }
    @Test
    public void should_getValidIndex_when_deleteChild(){
        //given
        int index = 0;
        Family family = new Family(father, mother);
        family.addChild(child1);
        family.addChild(child2);
        //when
        boolean deleted = family.deleteChild(index);
        Human[] result = family.getChildren();
        //then
        Human[] expected = new Human[] {child2};
        assertEquals(deleted, true);
        assertArrayEquals(result, expected);
    }
    @Test
    public void should_getValidObject_when_deleteChild(){
        //given
        Human childDel = new Human("name child2", "surname child2", 2020);
        Family family = new Family(father, mother);
        family.addChild(child1);
        family.addChild(child2);
        //when
        boolean deleted = family.deleteChild(childDel);
        Human[] result = family.getChildren();
        //then
        Human[] expected = new Human[] {child1};
        assertEquals(deleted, true);
        assertArrayEquals(result, expected);
    }
    @Test
    public void should_getNonValidObject_when_deleteChild(){
        //given
        Human childDel = new Human("name child3", "surname child3", 2020);
        Family family = new Family(father, mother);
        family.addChild(child1);
        family.addChild(child2);
        //when
        boolean deleted = family.deleteChild(childDel);
        Human[] result = family.getChildren();
        //then
        Human[] expected = new Human[] {child1, child2};
        assertEquals(deleted, false);
        assertArrayEquals(result, expected);
    }
}