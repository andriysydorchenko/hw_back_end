package App;
import java.util.Arrays;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children = new Human[0];
    private Pet pet;
    private int point = 0;
    public Family(Human mother, Human father){
        this.mother = mother;
        this.father = father;
    }

    //    getters
    public Human getMother() {
        return mother;
    }
    public Human getFather() {
        return father;
    }
    public Human[] getChildren() {
        return children;
    }
    public Pet getPet() {
        return pet;
    }

    //    setters
    public void setChildren(Human[] children) {
        this.children = children;
    }
    public void setFather(Human father) {
        this.father = father;
    }
    public void setMother(Human mother) {
        this.mother = mother;
    }
    public void setPet(Pet pet) {
        this.pet = pet;
        mother.setPet(pet);
        father.setPet(pet);
        for (int i = 0; i < children.length; i++) {
            children[i].setPet(pet);
        }
    }

    public void addChild(Human child) {
        Human[] temp = children;
        children = new Human[temp.length + 1];
        System.arraycopy(temp, 0, children, 0, point);
        children[point] = child;
        point++;
        child.setFamily(this);
    }
    public boolean deleteChild(int indexChild) {
        if(children.length > indexChild){
            children[indexChild].setFamily(null);
            Human[] temp = new Human[children.length];
            System.arraycopy(children, 0, temp, 0, children.length);
            point--;
            children = new Human[point];
            if(indexChild == 0) {
                System.arraycopy(temp, 1, children, 0, children.length);
                return true;
            }
            System.arraycopy(temp, 0, children, 0, indexChild);
            System.arraycopy(temp, indexChild, children, indexChild, temp.length-1-indexChild);
            return true;
        } else {
            return false;
        }
    }
    public boolean deleteChild(Human child) {
        for (int i = 0; i < children.length; i++) {
            if(child.equals(children[i])){
                children[i].setFamily(null);
                Human[] temp = new Human[children.length];
                System.arraycopy(children, 0, temp, 0, children.length);

                point--;
                children = new Human[point];
                if(i == 0) {
                    System.arraycopy(temp, 1, children, 0, children.length);
                    return true;
                }
                System.arraycopy(temp, 0, children, 0, i);
                System.arraycopy(temp, i, children, i, temp.length-1-i);
                return true;
            }
        }
        return false;
    }
    public int getCountFamily(){
        return 2+children.length;
    }
    @Override
    public String toString(){
        String str = """
                        father: %s
                        mother: %s
                        children: %s
                     """.formatted(father.toString(), mother.toString(), Arrays.toString(children));
        return str;
    }
    @Override
    protected void finalize(){
        System.out.println("deleted exemplar of class Family");
    }

}
