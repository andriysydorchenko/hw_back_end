package com.danit;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import src.danit.app.Human;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HumanTest {
    private static String expected1 = "Human{name='no name', surname='no surname', year=0, iq=0, mother=no mother, father=no father, pet= no pets}";
    private static String expected2 = "Human{name='name', surname='surname', year=10, iq=0, mother=no mother, father=no father, pet= no pets}";
    private static String expected3 = "Human{name='name', surname='surname', year=10, iq=0, mother=Mother2 SurMother2, father=Father2 SurFather2, pet= no pets}";
    private static int year = 10;
    private static Human father = new Human("Father2", "SurFather2", 1990);
    private static Human mother = new Human("Mother2", "SurMother2", 1990);

//    @Test
//    public void should_hasEmptyObject_when_toString(){
//        //given
//
//        //when
//        Human father1 = new Human();
//        //then
//        String expected = "Human{name='no name', surname='no surname', year=0, iq=0, mother=no mother, father=no father, pet= no pets}";
//        assertEquals(father1.toString(), expected);
//    }

//    @Test
//    public void should_hasNon
    @ParameterizedTest
    @MethodSource("arguments")
    public void should_getArrayWithTwoIndexes_when_sumOfTwoElements(String name, String surname, int year, int iq, Human father, Human mother, String expected) {
        //when
        Human human;
        if (name == null && surname == null){
            human = new Human();
        } else {
            human = new Human(name, surname, year, iq, mother, father, null, null, null);
        }
        //then
        assertEquals(human.toString(), expected);

    }

    public static Stream<Arguments> arguments() {
        return Stream.of(
                Arguments.of(null, null, 0, 0, null, null, expected1),
                Arguments.of("name", "surname", year, 0, null, null, expected2),
                Arguments.of("name", "surname", year, 0, father, mother, expected3)
        );
    }
}