package com.danit;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class FamilyService implements HumanCreator{
    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies(){
        return familyDao.getAllFamilies();
    }
    public List<Family> getFamiliesBiggerThan(int countMembers) {
        List<Family> filteredFamilies = familyDao.getFilteredFamilies(e -> e.getCountFamily() > countMembers);
        filteredFamilies.forEach(System.out::println);
        System.out.println(filteredFamilies.size());
        return filteredFamilies;
    }
    public List<Family> getFamiliesLessThan(int countMembers) {
        List<Family> filteredFamilies = familyDao.getFilteredFamilies(e -> e.getCountFamily() < countMembers);
        filteredFamilies.forEach(System.out::println);
        return filteredFamilies;
    }
    public int countFamiliesWithMemberNumber(int countMembers) {
        List<Family> filteredFamilies = familyDao.getFilteredFamilies(e -> e.getCountFamily() == countMembers);
        filteredFamilies.forEach(System.out::println);
        return filteredFamilies.size();
    }
    public void createNewFamily(Human mother, Human father){
        Family family = new Family(mother, father);
        familyDao.saveFamily(family);
    }
    public boolean deleteFamilyByIndex(int index){
        return familyDao.deleteFamily(index);
    }

    @Override
    public Family bornChild(Family family, String manName, String womanName) throws CloneNotSupportedException {
        ThreadLocalRandom random = ThreadLocalRandom.current();
        int sex = random.nextInt(2);
        String surname = family.getFather().getSurname();
        int iq = (family.getMother().getIq() + family.getFather().getIq()) / 2;
        Human father = family.getFather();
        Human mother = family.getMother();
        Pet pet = father.getPet();
        HashMap<String, String> schedule = new HashMap<>();
        Human man = new Man(manName,
                            surname,
                            0,
                            iq,
                            mother,
                            father,
                            pet,
                            schedule,
                            family);

        Human woman = new Woman(manName,
                                surname,
                                0,
                                iq,
                                mother,
                                father,
                                pet,
                                new HashMap<String, String>(),
                                family);

        Human child = sex == 1 ? man : woman;
        Family newFamily = (Family) family.clone();
        newFamily.addChild(child);
        familyDao.saveFamily(newFamily);
        return newFamily;
    }

    public Family adoptChild(Family family, Human child) throws CloneNotSupportedException {
        Family newFamily = (Family) family.clone();
        newFamily.addChild(child);
        familyDao.saveFamily(newFamily);
        return newFamily;
    }

    public void deleteAllChildrenOlderThen(int age) {
        List<Family> allFamilies = familyDao.getAllFamilies();
        allFamilies.forEach( family -> {
            List<Human> children = family.getChildren();
            Iterator<Human> iterator = children.iterator();
            while (iterator.hasNext()) {
                Human child = iterator.next();
                if (child.getYear() > age) {
                    family.deleteChild(child);
                    familyDao.saveFamily(family);
                }
            }
        });
    }
    public int count(){
        return familyDao.getAllFamilies().size();
    }
    public Family getFamilyByIndex(int index) {
      return familyDao.getFamilyByIndex(index);
    }
    public Set<Pet> getPets(int indexFamily) {
        return getFamilyByIndex(indexFamily).getPet();
    }
    public void addPet(int indexFamily, Pet pet){
        Family family = getFamilyByIndex(indexFamily);
        family.addPet(pet);
        familyDao.saveFamily(family);
    }
}
