package com.danit;

import java.util.HashMap;

public final class Man extends Human {


    public Man(String name, String surname, int year, int iq, Human mother, Human father, Pet pet, HashMap<String, String> schedule, Family family){
       super(name, surname, year, iq, mother, father, pet, schedule, family);
    }

    @Override
    public void greetPet() {
        System.out.printf("Hay, %s" , this.getPet().getNickname());
        System.out.println();
    }
    public void repairCar(){
        System.out.println("Car need to repair");
    }
}
