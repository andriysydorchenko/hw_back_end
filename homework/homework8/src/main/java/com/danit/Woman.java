package com.danit;

import java.util.HashMap;

public final class Woman extends Human {
    public Woman(String name, String surname, int year, int iq, Human mother, Human father, Pet pet, HashMap<String, String> schedule, Family family){
        super(name, surname, year, iq, mother, father, pet, schedule, family);
    }
    @Override
    public void greetPet() {
        System.out.printf("Hello, %s" , this.getPet().getNickname());
        System.out.println();
    }
    public void makeup(){
        System.out.println("I creating makeup");
    }
}
