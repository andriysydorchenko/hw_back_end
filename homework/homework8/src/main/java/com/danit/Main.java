package com.danit;
import java.util.HashSet;
import java.util.Set;

public class Main{
    public static void main(String[] args) throws CloneNotSupportedException {
        Set<String> habits = new HashSet<>();
        habits.add("habit1");
        habits.add("habit2");
        Pet roboCat1 = new RoboCat("roboCat1", 2, 80, habits);
        Pet dog1 = new Dog("dog1", 5, 15, habits);
        Pet dog2 = new Dog("dog2", 2, 90, habits);

        Human mother1 = new Human("mother1", "surname", 56, null, null);
        Human father1 = new Human("father1", "surname", 56, null, null);

        Woman woman1 = new Woman("nameMother1", "surnameMother1", 30, 160, mother1, father1, roboCat1, null, null);
        Human man1 = new Man("nameFather1", "surnameFather1", 30, 160, mother1, father1, dog1, null, null);
        Human woman2 = new Woman("nameMother2", "surnameMother2", 30, 160, mother1, father1, null, null, null);
        Human man2 = new Man("nameFather2", "surnameFather2", 30, 160, mother1, father1, dog1, null, null);

        Human child1 = new Woman("nameChild1", "surnameChild1", 20, 180, mother1, father1, null, null, null);
        Human child2 = new Man("nameChild2", "surnameChild2", 15, 120, mother1, father1, null, null, null);



        FamilyController familyController = new FamilyController();
        familyController.createNewFamily(woman1,man1);
        Family family1 = new Family(woman1, man1);
        familyController.bornChild(family1,"ChildMan1", "ChildWoman1");
        familyController.adoptChild(family1, child1);
        familyController.adoptChild(family1, child2);
        familyController.createNewFamily(woman2, man2);
        Family family2 = new Family(woman2, man2);
        familyController.bornChild(family2,"ChildMan2", "ChildWoman2");
        System.out.println(familyController.getAllFamilies());
        System.out.println();
        familyController.getFamiliesBiggerThan(4);
        familyController.getFamiliesLessThan(4);
        familyController.countFamiliesWithMemberNumber(5);
        System.out.println("Count families = " + familyController.count());
        System.out.println(familyController.getFamilyByIndex(1));
        familyController.addPet(1, dog2);
        System.out.println(familyController.getPets(1));
        System.out.println("deleteAllChildrenOlderThen");
        familyController.deleteAllChildrenOlderThen(15);
        System.out.println(familyController.getAllFamilies());
        familyController.deleteFamilyByIndex(0);
        System.out.println(familyController.getAllFamilies());

    }

}