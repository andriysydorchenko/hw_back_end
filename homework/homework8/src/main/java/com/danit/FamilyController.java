package com.danit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class FamilyController  {
    FamilyDao familyDao = new CollectionFamilyDao();
    FamilyService familyService = new FamilyService(familyDao);
    public List<Family> getAllFamilies(){
        return familyService.getAllFamilies();
    }
    public List<Family> getFamiliesBiggerThan(int countMembers) {
        return familyService.getFamiliesBiggerThan(countMembers);
    }
    public List<Family> getFamiliesLessThan(int countMembers) {
        return familyService.getFamiliesLessThan(countMembers);
    }
    public int countFamiliesWithMemberNumber(int countMembers) {
        return familyService.countFamiliesWithMemberNumber(countMembers);
    }
    public void createNewFamily(Human mother, Human father){
        familyService.createNewFamily(mother, father);
    }
    public boolean deleteFamilyByIndex(int index){
        return familyService.deleteFamilyByIndex(index);
    }


    public Family bornChild(Family family, String manName, String womanName) throws CloneNotSupportedException {
        return familyService.bornChild(family, manName, womanName);
    }

    public Family adoptChild(Family family, Human child) throws CloneNotSupportedException {
        return familyService.adoptChild(family,child);
    }

    public void deleteAllChildrenOlderThen(int age){
        familyService.deleteAllChildrenOlderThen(age);
    }
    public int count(){
        return familyService.count();
    }
    public Family getFamilyByIndex(int index) {
        return familyService.getFamilyByIndex(index);
    }
    public Set<Pet> getPets(int indexFamily) {
        return familyService.getPets(indexFamily);
    }
    public void addPet(int indexFamily, Pet pet){
        familyService.addPet(indexFamily, pet);
    }
}
